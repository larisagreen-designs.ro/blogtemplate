<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/all.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/layout.css">
</head>
<body class="d-flex flex-column min-vh-100">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Read<span>it</span>.</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    MENU
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Home<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Articles</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right" href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main class="article">
        <section class="hero">
            <div class="overlay">
                <div class="container col-md-9 text-center d-flex flex-column h-100 justify-content-center">
                    <h1>Blog Single</h1>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="row">
                <article class="col-md-8 pr-md-0">
                    <img src="/img/reading-time.jpg" class="img-fluid mb-5">
                    <h4>It is a long established fact a reader be distracted</h4>
                    <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the l
                        eap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
                        Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                    <img src="/img/coffee-reading.jpg" class="img-fluid mb-5">
                    <h4>It is a long established fact a reader be distracted</h4>
                    <p class="text-justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the l
                        eap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem
                        Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                    <div class="author-info row pt-4">
                        <img src="/img/author.jpg" class="img-fluid col-md-4">
                        <div class="col-md-8">
                            <h4>Anthony John Smith</h4>
                            <p class="text-justify ">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
                                The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here',
                                making it look like readable English.
                            </p>
                        </div>
                    </div>
                    <div class="comments pt-4">
                        <h5 class="mb-5">6 comments</h5>
                        <ul class="comments-list pl-0">
                            <li class="comment d-flex">
                                <div class="img-comment pr-4">
                                    <img src="/img/author.jpg">
                                </div>
                                <div class="comment-body">
                                    <h3>Anthony John Doe</h3>
                                    <div class="meta mb-3">July 19, 2021 at 2:21pm</div>
                                    <p class="text-justify">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of
                                        using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here</p>
                                </div>
                            </li>
                            <li class="comment d-flex">
                                <div class="img-comment pr-4">
                                    <img src="/img/author.jpg">
                                </div>
                                <div class="comment-body">
                                    <h3>Anthony John Doe</h3>
                                    <div class="meta mb-3">July 19, 2021 at 2:21pm</div>
                                    <p class="text-justify">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of
                                        using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here</p>
                                </div>
                            </li>
                            <li class="comment d-flex">
                                <div class="img-comment pr-4">
                                    <img src="/img/author.jpg">
                                </div>
                                <div class="comment-body">
                                    <h3>Anthony John Doe</h3>
                                    <div class="meta mb-3">July 19, 2021 at 2:21pm</div>
                                    <p class="text-justify">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of
                                        using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <h4>Leave a comment</h4>
                    <form class="contact-form px-md-5">
                        <div class="form-group">
                        <label for="exampleInputName">Name *</label>
                            <input type="name" class="form-control" id="exampleInputName">
                        </div>
                        <div class="form-group">
                        <label for="exampleInputEmail">Email *</label>
                            <input type="email" class="form-control" id="exampleInputEmail">
                        </div>
                        <div class="form-group">
                        <label for="exampleInputWebsite">Website</label>
                            <input type="text" class="form-control" id="exampleInputWebsite">
                        </div>
                        <div class="form-group">
                        <label for="exampleInputMessage">Message</label>
                            <textarea class="form-control" id="exampleFormControlTextarea" rows="6"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Post Comment</button>
                    </form>
                </article>
                <aside class="col-md-4">
                    <div class="input-group">
                        <input class="form-control py-2" type="search" id="example-search-input" placeholder="Type a keyword and hit enter" aria-label="Search">
                        <span class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </aside>
            </div>
        </section>
    </main>
    <footer class="mt-auto">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-3">
                    <a class="logo" href="#">Read<span>it</span>.</a>
                    <p class="pt-3">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, there live the blind texts.</p>
                    <div class="social-icons mt-5">
                        <a class="mr-2" href="twitter"><i class="fab fa-twitter fa-fw"></i></a>
                        <a class="facebook" href="facebook"><i class="fab fa-facebook-f fa-fw"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Latest News</p>
                    <div class="article-thumbnail d-flex">
                        <img src="/img/content-writer.jpg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                    <div class="article-thumbnail d-flex mt-4">
                        <img src="/img/blog-authors.jpeg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Information</p>
                    <ul class="pl-0">
                        <li><a href="#home"><i class="fas fa-chevron-right pr-2"></i>Home</a></li>
                        <li><a href="#about"><i class="fas fa-chevron-right pr-2"></i>About</a></li>
                        <li><a href="#articles"><i class="fas fa-chevron-right pr-2"></i>Articles</a></li>
                        <li><a href="#contact"><i class="fas fa-chevron-right pr-2"></i>Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Have a Question?</p>
                    <ul class="pl-0">
                        <li><i class="fas fa-map-marker-alt pr-3 pt-3"></i> <p class="d-inline">203 Fake St, Mountain View, San Francisco, California, USA</p></li>
                        <li><i class="fas fa-phone-alt pr-3 pt-3"></i> +2 392 3929 210</li>
                        <li><i class="fas fa-envelope pr-3 pt-3"></i> info@yourdomain.com</li>
                    </ul>
                </div>
            </div>
            <div class="text-center pt-5">
                <p class="copyright">Copyright ©2021 All rights reserved | This template is made by me</p>
            </div>
        </div>
    </footer>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/script.js"></script>
</body>
</html>
