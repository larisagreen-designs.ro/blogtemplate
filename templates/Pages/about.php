<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/all.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/layout.css">
</head>
<body class="d-flex flex-column min-vh-100">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Read<span>it</span>.</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    MENU
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Home<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Articles</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right" href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main class="about">
        <section class="hero">
            <div class="overlay">
                <div class="container col-md-9 text-center d-flex flex-column h-100 justify-content-center">
                    <h1>About</h1>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="row about-tab">
                <div class="col-md-6 pr-md-0 about-image">
                </div>
                <div class="col-md-6 px-md-5">
                    <span class="subheading">Welcome to Readit</span>
                    <h2 class="mt-2 mb-4">We give you the best articles you want.</h2>
                    <p class="extra">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia</p>
                    <div class="details mt-4">
                        <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="pills-our-mission-tab" data-toggle="pill" href="#pills-mission" role="tab" aria-controls="pills-mission" aria-selected="true">Our Mission</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-our-vision-tab" data-toggle="pill" href="#pills-vision" role="tab" aria-controls="pills-vision" aria-selected="false">Our Vision</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-our-value-tab" data-toggle="pill" href="#pills-value" role="tab" aria-controls="pills-value" aria-selected="false">Our Value</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-mission" role="tabpanel" aria-labelledby="pills-our-mission-tab">
                                <p class="py-4 px-4">"Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, they live the blind texts. Separated they live in
                                    Bookmarksgrove right at the coast of the Semantics, a large language ocean."
                                </p>
                            </div>
                            <div class="tab-pane fade" id="pills-vision" role="tabpanel" aria-labelledby="pills-our-vision-tab">
                                <p class="py-4 px-4">"Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, they live the blind texts. Separated they live in
                                    Bookmarksgrove right at the coast of the Semantics, a large language ocean."
                                </p>
                            </div>
                            <div class="tab-pane fade " id="pills-value" role="tabpanel" aria-labelledby="pills-our-value-tab">
                                <p class="py-4 px-4">"Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, they live the blind texts. Separated they live in
                                    Bookmarksgrove right at the coast of the Semantics, a large language ocean."
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 text-center">
                    <span class="subheading">Testimonial</span>
                    <h2 class="mt-3">Happy Clients</h2>
                </div>
            </div>
            <div class="row mt-5">
                <div class="owl-carousel owl-theme col-md-12">
                    <div class="item py-2">
                        <p class="font-weight-bold-500 text-justify">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, they live the blind texts.</p>
                        <div class="row mt-4 pb-1">
                            <div class="col-3">
                                <img src="/img/card1.jpg" class="rounded-circle">
                            </div>
                            <div class="col-9">
                                <p class="pt-3 mb-0 font-weight-bold-500">Roger Popescu</p>
                                <p class="font-weight-bold-500 text-color-primary">Manager</p>
                            </div>
                        </div>
                    </div>
                    <div class="item py-2">
                        <p class="font-weight-bold-500 text-justify">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, they live the blind texts.</p>
                        <div class="row mt-4 pb-1">
                            <div class="col-3">
                                <img src="/img/card1.jpg" class="rounded-circle">
                            </div>
                            <div class="col-9">
                                <p class="pt-3 mb-0 font-weight-bold-500">Roger Popescu</p>
                                <p class="font-weight-bold-500 text-color-primary">Manager</p>
                            </div>
                        </div>
                    </div>
                    <div class="item py-2">
                        <p class="font-weight-bold-500 text-justify">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, they live the blind texts.</p>
                        <div class="row mt-4 pb-1">
                            <div class="col-3">
                                <img src="/img/card1.jpg" class="rounded-circle">
                            </div>
                            <div class="col-9">
                                <p class="pt-3 mb-0 font-weight-bold-500">Roger Popescu</p>
                                <p class="font-weight-bold-500 text-color-primary">Manager</p>
                            </div>
                        </div>
                    </div>
                    <div class="item py-2">
                        <p class="font-weight-bold-500 text-justify">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, they live the blind texts.</p>
                        <div class="row mt-4 pb-1">
                            <div class="col-3">
                                <img src="/img/card1.jpg" class="rounded-circle">
                            </div>
                            <div class="col-9">
                                <p class="pt-3 mb-0 font-weight-bold-500">Roger Popescu</p>
                                <p class="font-weight-bold-500 text-color-primary">Manager</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer class="mt-auto">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-3">
                    <a class="logo" href="#">Read<span>it</span>.</a>
                    <p class="pt-3">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, there live the blind texts.</p>
                    <div class="social-icons mt-5">
                        <a class="mr-2" href="twitter"><i class="fab fa-twitter fa-fw"></i></a>
                        <a class="facebook" href="facebook"><i class="fab fa-facebook-f fa-fw"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Latest News</p>
                    <div class="article-thumbnail d-flex">
                        <img src="/img/content-writer.jpg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                    <div class="article-thumbnail d-flex mt-4">
                        <img src="/img/blog-authors.jpeg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Information</p>
                    <ul class="pl-0">
                        <li><a href="#home"><i class="fas fa-chevron-right pr-2"></i>Home</a></li>
                        <li><a href="#about"><i class="fas fa-chevron-right pr-2"></i>About</a></li>
                        <li><a href="#articles"><i class="fas fa-chevron-right pr-2"></i>Articles</a></li>
                        <li><a href="#contact"><i class="fas fa-chevron-right pr-2"></i>Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Have a Question?</p>
                    <ul class="pl-0">
                        <li><i class="fas fa-map-marker-alt pr-3 pt-3"></i> <p class="d-inline">203 Fake St, Mountain View, San Francisco, California, USA</p></li>
                        <li><i class="fas fa-phone-alt pr-3 pt-3"></i> +2 392 3929 210</li>
                        <li><i class="fas fa-envelope pr-3 pt-3"></i> info@yourdomain.com</li>
                    </ul>
                </div>
            </div>
            <div class="text-center pt-5">
                <p class="copyright">Copyright ©2021 All rights reserved | This template is made by me</p>
            </div>
        </div>
    </footer>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/script.js"></script>
</body>
</html>
