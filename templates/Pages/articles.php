<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/all.min.css">
    <link rel="stylesheet" href="/css/layout.css">
</head>
<body class="d-flex flex-column min-vh-100">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Read<span>it</span>.</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    MENU
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Articles</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right" href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main>
        <section class="hero">
            <div class="overlay">
                <div class="container d-flex flex-column h-100 justify-content-center">
                    <p>Hello! Welcome to</p>
                    <h1>Readit blog</h1>
                    <p class="w-50">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, there live the blind texts. Separated they live in Bookmarksgrove right at the
                        coast af the Semantics, a large language ocean.
                    </p>
                    <i class="fas fa-arrow-down text-white"></i>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="row">
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title"><a href="#">Card title</a></h3>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card">
                        <div class="top position-relative">
                            <img src="https://miro.medium.com/fit/c/470/353/1*_ed9L5YlfdKC-D_THYCdJQ@2x.jpeg" class="card-img-top" height="300" alt="...">
                            <div class="date position-absolute d-flex align-items-center">
                                <div class="one py-1 pl-3 pr-3">
                                    <span class="day">18</span>
                                </div>
                                <div class="two pl-0 pr-3 py-1">
                                    <span class="year d-block">2021</span>
                                    <span class="month">July</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="#">Card title</a></h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#"><i class="fas fa-arrow-right"></i> READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1<span class="sr-only">(current)</span></a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </section>
    </main>
    <footer class="mt-auto">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-3">
                    <a class="logo" href="#">Read<span>it</span>.</a>
                    <p class="pt-3">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, there live the blind texts.</p>
                    <div class="social-icons mt-5">
                        <a class="mr-2" href="twitter"><i class="fab fa-twitter fa-fw"></i></a>
                        <a class="facebook" href="facebook"><i class="fab fa-facebook-f fa-fw"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Latest News</p>
                    <div class="article-thumbnail d-flex">
                        <img src="/img/content-writer.jpg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                    <div class="article-thumbnail d-flex mt-4">
                        <img src="/img/blog-authors.jpeg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Information</p>
                    <ul class="pl-0">
                        <li><a href="#home"><i class="fas fa-chevron-right pr-2"></i>Home</a></li>
                        <li><a href="#about"><i class="fas fa-chevron-right pr-2"></i>About</a></li>
                        <li><a href="#articles"><i class="fas fa-chevron-right pr-2"></i>Articles</a></li>
                        <li><a href="#contact"><i class="fas fa-chevron-right pr-2"></i>Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Have a Question?</p>
                    <ul class="pl-0">
                        <li><i class="fas fa-map-marker-alt pr-3 pt-3"></i> <p class="d-inline">203 Fake St, Mountain View, San Francisco, California, USA</p></li>
                        <li><i class="fas fa-phone-alt pr-3 pt-3"></i> +2 392 3929 210</li>
                        <li><i class="fas fa-envelope pr-3 pt-3"></i> info@yourdomain.com</li>
                    </ul>
                </div>
            </div>
            <div class="text-center pt-5">
                <p class="copyright">Copyright ©2021 All rights reserved | This template is made by me</p>
            </div>
        </div>
    </footer>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
</body>
</html>