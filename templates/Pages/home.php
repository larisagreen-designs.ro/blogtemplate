<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/all.min.css">
    <link rel="stylesheet" href="/css/layout.css">
</head>
<body class="d-flex flex-column min-vh-100">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Read<span>it</span>.</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    MENU
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Articles</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right mr-4" href="#">Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-left text-lg-right" href="#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main>
        <section class="hero">
            <div class="overlay">
                <div class="container d-flex flex-column h-100 justify-content-center">
                    <p>Hello! Welcome to</p>
                    <h1>Readit blog</h1>
                    <p class="w-50">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, there live the blind texts. Separated they live in Bookmarksgrove right at the
                        coast af the Semantics, a large language ocean.
                    </p>
                    <i class="fas fa-arrow-down text-white"></i>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="row">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card17.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card15.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card18.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card16.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card5.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card6.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card14.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card1.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12 article-card">
                    <div class="row">
                        <img src="/img/card8.jpg" class="card-img-top col-md-8 w-100" alt="#">
                        <div class="body col-md-4">
                            <p class="text"><small class="text-muted">ILLUSTRATION</small></p>
                            <h2><a href ="#">Some quick example text to build on the card</a></h2>
                            <ul class="social d-flex pl-0">
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/" class="blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <p class="text"><small class="text-muted">11/03/2021 | 12 min read</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer class="mt-auto">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-3">
                    <a class="logo" href="#">Read<span>it</span>.</a>
                    <p class="pt-3">Far far away, behind the word mountains, far from the countries Vokalia and Consonatia, there live the blind texts.</p>
                    <div class="social-icons mt-5">
                        <a class="mr-2" href="twitter"><i class="fab fa-twitter fa-fw"></i></a>
                        <a class="facebook" href="facebook"><i class="fab fa-facebook-f fa-fw"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Latest News</p>
                    <div class="article-thumbnail d-flex">
                        <img src="/img/content-writer.jpg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                    <div class="article-thumbnail d-flex mt-4">
                        <img src="/img/blog-authors.jpeg" alt="...">
                        <div class="body">
                            <h5 class="title">Even the all-powerful Pointing has no control about</h5>
                            <p class="text"><small class="text-muted">Oct. 16, 2019 Admin 19</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Information</p>
                    <ul class="pl-0">
                        <li><a href="#home"><i class="fas fa-chevron-right pr-2"></i>Home</a></li>
                        <li><a href="#about"><i class="fas fa-chevron-right pr-2"></i>About</a></li>
                        <li><a href="#articles"><i class="fas fa-chevron-right pr-2"></i>Articles</a></li>
                        <li><a href="#contact"><i class="fas fa-chevron-right pr-2"></i>Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-xl-3">
                    <p class="mt-4 mt-lg-3">Have a Question?</p>
                    <ul class="pl-0">
                        <li><i class="fas fa-map-marker-alt pr-3 pt-3"></i> <p class="d-inline">203 Fake St, Mountain View, San Francisco, California, USA</p></li>
                        <li><i class="fas fa-phone-alt pr-3 pt-3"></i> +2 392 3929 210</li>
                        <li><i class="fas fa-envelope pr-3 pt-3"></i> info@yourdomain.com</li>
                    </ul>
                </div>
            </div>
            <div class="text-center pt-5">
                <p class="copyright">Copyright ©2021 All rights reserved | This template is made by me</p>
            </div>
        </div>
    </footer>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
</body>
</html>